﻿



angular.module("umbraco")
    .controller("Umbraco.SPCPropertyEditors.Grid.ContentController",
    function ($scope, $rootScope, $timeout, dialogService) {

        $scope.setContentId = function () {

            dialogService.contentPicker({
                multiPicker: false,
                callback: function (data) {

                    $scope.control.value = {
                        id: data.id,
                        name: data.name,
                        path: data.path
                    };
                    $scope.setPreview();
                }
            });
        };

        $scope.setPreview = function () {

            if ($scope.control.value.id) {
                $scope.id = $scope.control.value.id;
                $scope.name = $scope.control.value.name;
                $scope.path = $scope.control.value.path;
            }
        };

        $timeout(function () {
            if ($scope.control.$initializing) {
                $scope.setContentId();
            } else {
                $scope.setPreview();
            }
        }, 200);
    });




