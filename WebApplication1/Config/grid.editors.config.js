﻿[
  {
    "name": "Rich text editor",
    "alias": "rte",
    "view": "rte",
    "icon": "icon-article"
  },
  {
    "name": "Image",
    "alias": "media",
    "view": "media",
    "icon": "icon-picture"
  },
  {
    "name": "Image wide",
    "alias": "media_wide",
    "view": "media",
    "render": "/App_Plugins/Grid/Editors/Render/media_wide.cshtml",
    "icon": "icon-picture"
  },
  {
    "name": "Image wide cropped",
    "alias": "media_wide_cropped",
    "view": "media",
    "render": "media",
    "icon": "icon-picture",
    "config": {
      "size": {
        "width": 1920,
        "height": 700
      }
    }
  },
  {
    "name": "Image rounded",
    "alias": "media_round",
    "view": "media",
    "render": "/App_Plugins/Grid/Editors/Render/media_round.cshtml",
    "icon": "icon-picture"
  },
  {
    "name": "Image w/ text right",
    "alias": "media_text_right",
    "view": "/App_Plugins/Grid/Editors/Views/media_with_description.html",
    "render": "/App_Plugins/Grid/Editors/Render/media_text_right.cshtml",
    "icon": "icon-picture"
  },
  {
    "name": "Macro",
    "alias": "macro",
    "view": "macro",
    "icon": "icon-settings-alt"
  },
  {
    "name": "Embed",
    "alias": "embed",
    "view": "embed",
    "render": "/App_Plugins/Grid/Editors/Render/embed_videowrapper.cshtml",
    "icon": "icon-movie-alt"
  },
  {
    "name": "Banner Headline",
    "alias": "banner_headline",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
      "style": "font-size: 36px; line-height: 45px; font-weight: bold; text-align:left",
      "markup": "<h2 style='font-size:62px;text-align:left'>#value#</h2>"
    }
  },
  {
    "name": "Banner Tagline",
    "alias": "banner_tagline",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
      "style": "font-size: 25px; line-height: 35px; font-weight: normal; text-align:center",
      "markup": "<h2 style='font-weight: 100; font-size: 40px;text-align:center'>#value#</h2>"
    }
  },
  {
    "name": "Headline",
    "alias": "headline",
    "view": "textstring",
    "icon": "icon-coin",
    "render": "/App_Plugins/Grid/Editors/Render/headline.cshtml"
  },
  {
      "name": "Headline with subtitle",
      "alias": "headline with subtitle",
      "view": "/App_Plugins/Grid/Editors/Views/headline_with_subtitle.html",
      "icon": "icon-coin",
      "render": "/App_Plugins/Grid/Editors/Render/headline_with_subtitle.cshtml"
  },
  {
    "name": "Headline centered",
    "alias": "headline_centered",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
      "style": "font-size: 30px; line-height: 45px; font-weight: bold; text-align:center;",
      "markup": "<h1 style='text-align:center;'>#value#</h1>"
    }
  },
  {
    "name": "Abstract",
    "alias": "abstract",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
      "style": "font-size: 16px; line-height: 20px; font-weight: bold;",
      "markup": "<h3>#value#</h3>"
    }
  },
  {
    "name": "Paragraph",
    "alias": "paragraph",
    "view": "textstring",
    "icon": "icon-font",
    "config": {
      "style": "font-size: 16px; line-height: 20px; font-weight: light;",
      "markup": "<p>#value#</p>"
    }
  },
  {
    "name": "Testimonial",
    "alias": "testimonial",
    "view": "/App_Plugins/Grid/Editors/Views/testimonial_quote.html",
    "render": "/App_Plugins/Grid/Editors/Render/testimonial_quote.cshtml",
    "icon": "icon-quote"
  },
  {
    "name": "Code",
    "alias": "code",
    "view": "textstring",
    "icon": "icon-code",
    "config": {
      "style": "overflow: auto;padding: 6px 10px;border: 1px solid #ddd;border-radius: 3px;background-color: #f8f8f8;font-size: .9rem;font-family: 'Courier 10 Pitch', Courier, monospace;line-height: 19px;",
      "markup": "<pre>#value#</pre>"
    }
  },
  {
    "name": "Doc Type",
    "alias": "docType",
    "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
    "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
    "icon": "icon-item-arrangement",
    "config": {
      "allowedDocTypes": [],
      "nameTemplate": "",
      "enablePreview": true,
      "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
      "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
      "previewCssFilePath": "",
      "previewJsFilePath": ""
    }
  },
  {
      "name": "Multi Picker",
      "alias": "multipicker",
      "view": "/app_plugins/GridContentPicker/gridportfolioeditor.html",
      "icon": "icon-coin",
      "render": "/Views/Partials/Grid/Editors/Portfolio.cshtml"
  }
]