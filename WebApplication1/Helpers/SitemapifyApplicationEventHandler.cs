﻿using Sitemapify;
using Sitemapify.Models;
using Sitemapify.Umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace WebApplication1.Helpers
{
    public class SitemapifyApplicationEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Configure.With(config => config
              .UsingContentProvider(new CustomXmlSiteMapContentProvider())
            );
        }

        protected override bool ExecuteWhenApplicationNotConfigured { get; } = false;
        protected override bool ExecuteWhenDatabaseNotConfigured { get; } = false;
    }

    public class CustomXmlSiteMapContentProvider : SitemapifyUmbracoContentProvider
    {
        protected override SitemapUrl CreateSitemapUrlForContent(IPublishedContent content)
        {
            return SitemapUrl.Create(content.UrlAbsolute(), content.UpdateDate, SitemapChangeFrequency.Monthly, 1.0);
        }
    }
}