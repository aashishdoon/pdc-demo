﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace WebApplication1.Helpers
{
    public class Helper
    {
        public static IEnumerable<IPublishedContent> GetChildPagesForNavigation(IPublishedContent page)
        {
            return page.Children.Where("Visible").Where(x => x.TemplateId != 0);
        }
    }
}